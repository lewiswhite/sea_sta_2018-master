package qa.java.lab.hibernate;
import javax.persistence.*;
import java.util.*;

// TO DO: define this class as an entity
// and map it to the appropriate database table
public class BankAccount implements java.io.Serializable {

	// TO DO: define this as the primary key (unique identifier)
	// TO DO: map this variable to a table column
	private int accountNumber;
	
	// TO DO: map this variable to a table column
	private String accountName;
	// TO DO: map this variable to a table column
	private double balance;

	
	//every bank account has a list of transactions
	// N.B. the mappedBy attribatue in the annotation is 
	// the instance variable in BankTransaction that corresponds to the Foreign Key

	// TO DO: Define the association to the BankTransaction class (the target entity) as 
	// 	ONE TO MANY. Specify a cascade strategy of CascadeType.ALL
	

	@OrderBy("trans_no")
	private Set<BankTransaction> transactions;
	
	
	public Set<BankTransaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<BankTransaction> transactions) {
		this.transactions = transactions;
	}

	public BankAccount(){ }

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	/* business methods follow (along with an inner class) */
	
	protected boolean sufficentFundsAvailable(double amount){
		return (amount <= balance);
	}
	
	public void withdraw(double withdrawalAmount) throws InsufficientFundsException {
		// check there's enough money
		if (sufficentFundsAvailable(withdrawalAmount)){

			// TO DO:
			// generate a bank transaction
			// set all properties EXCEPT the primary key, which will be auto-generated
			// Remember to attach the bank transaction to this account (the transaction
			// contains an account no.).
			//
			// add the transaction to the set, and update balance

		} else {
			// not enough money, so...
			throw new InsufficientFundsException(this.accountNumber, this.accountName);
		}
	}

	
	public void deposit(double amount) {

		// TO DO:
		// generate a bank transaction
		// set all properties EXCEPT the primary key, which will be auto-generated
		// Remember to attach the bank transaction to this account (the transaction
		// contains an account no.).
		//
		// add the transaction to the set, and update balance

	}
	
	// static inner class to define an Exception condition...
	static class InsufficientFundsException extends Exception {
		
		public InsufficientFundsException() { super();}
		
		public InsufficientFundsException(int accountNumber, String accountName) {
			super("Insufficient funds to support requested operation on account no. "+
			accountNumber + " name: " + accountName);
		}
		
	} // end class InsufficientFundsException
	
	
}// end class BankAccount
