/* Class for Customer */
package qa.java.lab.customer1;

public class Customer {
	//
	// ToDo:
	//
	// Declare two public variables to hold customer's name and age



	//
	// ToDo:
	//
	// Provide a constructor that takes a String and an int argument
	// Use the arguments to initialise the instance variables for the customer's name
	// and account number, respectively.
	//




	//
	// ToDo:
	//
	// Provide a method that returns the customer's name
	// Call it getName





	//
	// ToDo:
	//
	// Provide a method that returns the customer's account number
	// Think of a suitable name





	//
	// ToDo:
	//
	// Provide a method that updates the customer's name
	// This method will ignore attempts to set the value to null or to a zero-length String
	//
	// myString = ""; is an example of a zero-length String...
	//
	// remember Strings have a length() method



}
