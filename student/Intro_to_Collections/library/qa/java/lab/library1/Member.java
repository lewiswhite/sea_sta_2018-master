package qa.java.lab.library1;

public class Member
{
    private String name;
    private int age, idNum;

    private static int nextIDNumber = 1000;

    public Member(String mName, int mAge)
    {
    	  name = mName;
        age = mAge;
        idNum = ++nextIDNumber;
    }

    public String getName()
    {
    	return name;
    }

    public String getDetails()
    {
    	  StringBuffer details = new StringBuffer(idNum + "\t" + name);
        details.append((name.length() > 7)? "\t": "\t\t");
        details.append("" + age);
    	return details.toString();
    }
}
