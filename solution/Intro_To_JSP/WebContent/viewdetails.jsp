<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ page import="java.util.*" %>
</head>

<BODY>

<DIV align="center">
<H1>Festival of Culture</H1>
</DIV>



<DIV align="left">
<H2>Your details: </H2>

<TABLE border="0">
<TR>
<TD width="100" align="right"><B>Name:</B></TD>
<TD align="left"><%= request.getParameter("fullname") %></TD>
</TR>
<TR>
<TD width="100" align="right"><B>Age:</B></TD>
<TD align="left"><%= request.getParameter("age") %></TD>
</TR>
<TR>
<TD width="100" align="right"><B>Email:</B></TD>
<TD align="left"><%= request.getParameter("email") %></TD>
</TR>
<TR>
<TD width="100" align="right" valign="top"><B>Interests:</B></TD>
<TD align="left">
<% String[] interests = request.getParameterValues("interest");
	if (interests != null && interests.length > 0) { %>
	<UL>
	<% for (int i = 0; i < interests.length; i++) { %>
		<LI><%= interests[i] %></LI>
	<% } %>
	</UL>
<% } else { %>
	<I>none</I><% } 
%>
</TD>
</TR>
</TABLE>
</DIV>
<BR><BR><HR><BR>

<DIV>
Contact details: 
<A href="mailto:<%= application.getInitParameter("email") %>"><%= application.getInitParameter("email") %></A>.
<BR>This page was processed at <%= new java.util.Date() %>.
</DIV>

</body>
</html>



