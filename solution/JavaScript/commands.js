// Display items length, first item details

console.log(items.length);
console.dir(items[0]);

// Define a method to return Customer details

cust.getDetails = function() {
	return this.name + ", from " + this.town;
};

console.log(cust.getDetails());

// Function to find most expensive item by name

var mostExpensiveItemName = function() {
	if (items.length==0) return false;
	var maxPriceSoFar = 0;
	var maxItemSoFarIndex = 0;
	for (var i=0; i<items.length; i++) {
		if (items[i].price > maxPriceSoFar) {
			maxPriceSoFar = items[i].price;
			maxItemSoFarIndex = i;
		}
	}
	return items[maxItemSoFarIndex].productName;
};

console.log(mostExpensiveItemName());

// Add new Item literally, to end of array

items[items.length] = {productId:1234,productName:"Leather Jacket",price:50.99,image:"leather.gif"};

console.log(mostExpensiveItemName);

// Define a Shopping cart object literal: customer, items, addItem()

var cart = {
	customer: cust,
	cartItems: [],
	addItem: function (item) {
		this.cartItems[this.cartItems.length] = item;
	}
};

console.dir(cart);

// Add a couiple of items

cart.addItem(items[2]);
cart.addItem(items[5]);

console.dir(cart);

// Declare a method getPrice()

cart.getPrice = function() {
	var total = 0;
	for (var i=0; i<this.cartItems.length; i++) {
		total += this.cartItems[i].price;
	}
	return total;
};

console.log(getPrice());

// Re-assign the getPrice() function, and call this

var getPrice2 = cart.getPrice;

console.log(getPrice2());
console.log(getPrice2);
console.dir(getPrice2);

console.log(getPrice2.call(cart));

// Define inflation function, call it

function hyperInflation(item,f) {
	for (var i=0; i<12; i++) {
		item.price += 1.0;
		console.log(f());
	}
}

// Call hyperinflation with one of the items, and the mostExpensiveItemName function

hyperInflation(items[5],mostExpensiveItemName);

// Call hyperinflation with one of the (cart) items, and the getPrice function

hyperInflation(items[5],cart.getPrice);



