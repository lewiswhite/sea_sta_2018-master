package qa.java.sol.applet;

import java.applet.Applet;
import java.awt.*;

/*
 *
 * SimpleApplet
 *
 */
public class SimpleApplet extends Applet
{
	private String s = "Default string";
        private Image pic;

	public void init()
	{
		String str = getParameter("MyMessage");
		if (str	!= null)
		{
			s = str;
		}
                pic = getImage(getDocumentBase(),"media/picasso.gif");

	}

	public void paint(Graphics g)
	{
		g.drawString("SimpleApplet: " + s, 1, 10);
                g.drawImage(pic, 10, 10, this);
	}

}

