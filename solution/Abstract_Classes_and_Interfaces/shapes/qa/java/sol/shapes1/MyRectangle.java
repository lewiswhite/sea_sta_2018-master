package qa.java.sol.shapes1;

import java.awt.*;

/*
 *
 * MyRectangle
 *
 */

public class MyRectangle extends MyShape
{
	//
	// Constructor
	//
	public MyRectangle(int l, int t, int w, int h)
	{
		super(l, t, w, h);
	}


	//
	// Must implement draw method defined in MyShape
	//
	public void draw(Graphics g)
	{
		//
		// Draw the rectangle
		//
		g.drawRect(left,top,width,height);
	}
}
