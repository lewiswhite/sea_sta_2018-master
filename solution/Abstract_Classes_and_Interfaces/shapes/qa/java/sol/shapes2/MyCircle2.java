package qa.java.sol.shapes2;

import java.awt.*;

/*
 *
 * MyCircle2
 *
 */

public class MyCircle2 extends MyShape2 implements Drawable
{
	//
	// Constructor
	//
	public MyCircle2(int centreX, int centreY, int radius)
	{
		//
		// Convert the values to those held by
		// the superclass (i.e. left, top, width, height)
		//
		super(centreX - radius,
			  centreY - radius,
			  radius * 2,
			  radius * 2);
	}


	//
	// Must implement draw method defined in Drawable interface
	//
	public void draw(Graphics g)
	{
		//
		// Draw our circle
		//
		g.drawOval(left, top, width, height);
	}
}
