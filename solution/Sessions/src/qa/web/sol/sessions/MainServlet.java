package qa.web.sol.sessions;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @author lewis
 * 
 */
public class MainServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			
		resp.setContentType("text/html");

		String body = null;

		String path = req.getServletPath();
		
		if (path.equals("/register")) {
			body = "/register.jsp";
		} else if (path.equals("/login")) {
			body = "/login.jsp";
		} else if (path.equals("/nextyear")) {
			body = "/nextyear.jsp";
		} else if (path.equals("/loggedin")) {
			body = "/loggedin.jsp";
		} else if (path.equals("/registerresults")) {
			body = "/registerresults.jsp";
		} else if (path.equals("/viewdetails")) {
			body = "/viewdetails.jsp";
		} else {
			body = "/information.jsp";
		}

		ServletContext ctx = getServletContext();
		
		RequestDispatcher rd = null;
		
		rd = ctx.getRequestDispatcher("/header.jsp");
		rd.include(req, resp);

		rd = ctx.getRequestDispatcher(body);
		rd.include(req, resp);

		rd = ctx.getRequestDispatcher("/footer.jsp");
		rd.include(req, resp);

	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGet(req, resp);
	}

}
