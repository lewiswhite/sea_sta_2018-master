package qa.web.sol.sessions;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import java.text.*;

/**
 * @version 	1.0
 * @author lewis
 */
public class RegisterServlet extends HttpServlet {

	private static final NumberFormat nf = new DecimalFormat("00000");

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		String fullname = req.getParameter("fullname");
		if (fullname != null) {
			Cookie cookie1 = new Cookie("fullname", fullname);
			cookie1.setMaxAge(30000000);
			resp.addCookie(cookie1);
		}
		String email = req.getParameter("email");
		if (email != null) {
			Cookie cookie2 = new Cookie("email", email);
			cookie2.setMaxAge(30000000);
			resp.addCookie(cookie2);
		}
			String age = req.getParameter("age");
		if (age != null) {		
			Cookie cookie3 = new Cookie("age", age);
			cookie3.setMaxAge(30000000);
			resp.addCookie(cookie3);
		}
		String[] interests = req.getParameterValues("interest");
		if (interests != null) {		
			StringBuffer interestStringBuffer = new StringBuffer(50);
			for (int i = 0; i < interests.length; i++) {
				interestStringBuffer.append(interests[i] + " ");
			}
			Cookie cookie4 = new Cookie("interests", interestStringBuffer.toString());
			cookie4.setMaxAge(30000000);
			resp.addCookie(cookie4);
		}
		String username = req.getParameter("username");
		if (username == null || username.equals("")) {
			resp.sendRedirect("register");
		} else {
			String password = generatePassword(username);
			System.out.println("The password is: " + password);
			req.setAttribute("password", password);
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/registerresults");
			rd.forward(req, resp);
		}
	}

	/**
	* @see javax.servlet.http.HttpServlet#void (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGet(req, resp);
	}
	
	private String generatePassword(String username) {
		if (username == null) return null;
		int hash = Math.abs(username.hashCode() % 100000);
		String hashString = nf.format(hash);
		return hashString;
	}

}
