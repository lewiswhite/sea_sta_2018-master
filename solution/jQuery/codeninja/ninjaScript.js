
$(document).ready(function(){
	
	
	// 1. Hide the paragraph elements: two versions shown
	//===================================================
	
	//initially hide all paragraph elements
	//$("p").hide();
	
	
	//better... hide all elements with class = "description"
	$(".description").hide();
	
	
	
	
	//2. The paragraph we want to reveal is the next sibling of the <h2>
	//=================================================================
	//event handler for click events on all <h2> elements
	//
	$("h2").click(function(){
		//reveal or hide the next sibling element
		//over a one-second duration
		$(this).next().toggle(1000);
	});
	
	
	//3. Use two mouse-event handlers on <h2>
	//========================================
	//use mouse-event handlers to change the colour of <h2> elements
	$("h2").mouseover(function(){
		// change the CSS of the element
		$(this).css("color", "yellow");
	});
	$("h2").mouseout(function(){
		// change the CSS of the element
		$(this).css("color", "white");
	});
	
	
	
	/*
	//4. Alternative to part 2, using a Boolean var
	//=============================================
	
	// here we toggle visiblity using a variable
	var hidden = true;
	$("h2").click(function(){
		//reveal or hide the next sibling element
		//over a one-second duration
		if (hidden){
			$(this).next().slideDown(1000);			
		} else {
			$(this).next().slideUp(1000);						
		}
		//reverse the value in the Boolean variable
		hidden = !hidden;
	});
	*/
	
	//5. Select every other cell in the table and set to an alternate colour
	//======================================================================
	
	//Lighten the background colour of alternating columns in table
	//This technique gives alternating columns only if the number of columns is even;
	//with an odd number of columns, this technique results in a chessboard effect.
	//
	
	$("tr td:even").css("background-color", "#117711");
	
	
	
	//6. Change the background colour of a row while it's being pointed at
	//===================================================================
	
	//note that the hover function combines mouseover and mouseout: it
	//therefore requires two functions as arguments, one for pointer moving onto
	//the element and one for leaving the element
	$("tbody tr").hover(function(){
		$(this).css("background-color", "#E05500");
	}, function(){
		$(this).css("background-color", "#004422");
	});

 	
});